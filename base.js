﻿var c = document.getElementById('canvas').getContext('2d'),	//The Canvas Context to draw on
	particles = [],											//All particles (ammo) that should be drawn
	ghosts = [],											//All ghosts that should be drawn
	explosions = [],										//All explosions that should be drawn
	infoTexts = [],											//All infotexts that should be drawn
	guns = [],												//All living guns (self + opponents)
	deadVessels = [],										//All dead guns (self + opponents)
	scoreDisplay,
	timeDisplay,
	character,
	delay,
	ticks = 0,												//Current game time
	timeCount = 0,
	score = 0,													//Score in game 
	gameRunning = false,									//If the game is currently running
	gameSM = 0,                           //State Machine
	flagsEnterDebounce = 0,
	loop,													   //The drawing loop
	loopTimer,
	socket,													//The webserver connection
	explosionImg,											//The image (sprite) used for an explosion
	implosionImg,											//The image (sprite) used for another explosion
	gunosionImg,											//The image (sprite) used for another explosion
	ghostImg = [],											    //The image used for an ghost
	bulletHeartImg,
	Blabel,
	Tlabel,
	gunImg,
	bgStartEndImg,
	textLabelImg,
  startLabelImg,
  endLabelImg,
  charactersImg = [],
  charactersIdx,
  textImg = [],
  textIdx,
  effectImg = [],
  effectIdx,
  cardImg = [],
  cardIdx,
  timeDelayCount,
  gameOverLabelImg,
	n,
	primaryColors = ['127,255,0','255,102,153','148,0,211','139,69,19'],	//The color palette (first colors)
	secondaryColors = ['192,255,62','255,51,51','138,43,226','160,82,45'],	//The color palette	(second colors for gradients)
	multiPlayer = false;									//GameMode (SinglePlayer w/o Network, Multiplayer)
	
var MAX_AMMO = 100,										//Maximum amount of ammo that can be carried
		INIT_COOLDOWN = 5,									//Cooldown between two shots
		MAX_LIFE = 10000,										//Maximum amount of life (of the gun)
		MAX_PARTICLE_TIME = 200,							//Maximum time that a particle (ammo) is alive
		ghost_LIFE = 10,									//Initial life of an ghost
		MAX_SPEED = 6,										//Maximum speed of the sheep
		DAMAGE_FACTOR = 12,									//Maximum damage per shot
		ACCELERATE = 0.1,									//Acceleration per round
		FIX_WIDTG = c.canvas.width *20/100,
		FIX_HEIGHT = c.canvas.height *25/100;
		TIME_OUT  = 60;  										// Timeout 2 min
		DEFAULT_DELAY = 1000; 							// 1 s


var characArrayText = 
[
["...จะทำก็ทำได้นิ....นี้ของรางวัล","รอบหน้าพยายามให้มากกว่านี้...เข้าใจ?..."],
["อีกนิดเดียวเอง ลองอีกตาก็ได้นะครับ!","และนี่....ของรางวัลของรอบนี้ครับ"],
["What a shoot! Here's your price,","keep up the good work <3"],
["ฮ่ะๆ อ๊ะนี้ของรางวัล แล้วก็อย่ากังวลไป","ไม่ว่าจะแพ้หรือชนะ ความรักน่ะก็ชนะทุกสิ่ง!"],
["สุดยอด! ทำได้ดี ลองเล่นกันอีกตาไหมคะ?","ถ้ากลัวผีล่ะก็นึกถึงหน้าฉันไว้สิヾ(。◣∀◢。)ﾉ"]
];


var handlefocus = function(e) {	//Gets focus to the canvas
	if (e.type === 'mouseover')
		c.canvas.focus();
	else 
		return true;
	return false;
};

var flags_enter_debounce = 0;

var manageKeyboard = function(key, status) {	//Manages the keyboard
	switch(key) {
		case 37:
			keyboard.left = status;
			break;
		case 38:
			//keyboard.up = status;
			break;
		case 39:
			keyboard.right = status;
			break;
		case 40:
			//keyboard.down = status;
			break;
		case 32:
			keyboard.shoot = status;
			break;
		case 13:
		// if(status)
		// {
		// 	if(flagsEnterDebounce == 0){
				
		// 		setEnterState();
		// 		flagsEnterDebounce = 1;

		// 		setTimeout(function() {
	 //  			 flagsEnterDebounce = 0;
		// 			}, 2000);
		// 	}
		// }
			break;
		default:
			return;	//Not a meaningful key - just return!
	}
};