﻿infoText.prototype.draw = function() {
	c.save();
	c.translate(this.x, this.y);
	c.textAlign = 'center';
	c.fillStyle = 'rgba(' + this.color + ', ' + (this.time / this.total) + ')';
	c.fillText(this.text, 0, 0); 
	c.restore();
};

scoreDisplay.prototype.draw = function() {
	c.save();
	c.translate(this.x, this.y);
	c.drawImage(BlabelImg, 0, 0, BlabelImg.width/1.5, BlabelImg.height/1.5);
	c.restore();

 	c.save();
	c.translate(this.x+BlabelImg.width/(1.5*2), this.y+BlabelImg.height/(1.5*2));
	c.font = "35px Verdana";
	c.textAlign = 'center';
	c.fillStyle = "#F8F8FF";
	c.fillText(this.text, 0, 0); 
	c.restore();
};

timeDisplay.prototype.draw = function() {
	c.save();
	c.translate(this.x-40, this.y-40);
	c.drawImage(TlabelImg, 0, 0, TlabelImg.width/1.5, TlabelImg.height/1.5);
	c.restore();

	c.save();
	c.translate(this.x+2, this.y+17);
	c.font = "30px Verdana";
	c.textAlign = 'center';
	c.fillStyle = "#FF0000";

  if(this.time == undefined)
  {

  }
  else
  {
  	c.fillText(this.time, 0, 0);
  }
  c.restore();
};


character.prototype.draw = function() {
	c.save();
	c.translate(this.x, this.y);
	c.drawImage(ghostImg[1], 0, 100, ghostImg[1].width/3, ghostImg[1].height/3);
	c.restore();
}


var imgDrawing = function(x,y,img,w,h)
{
	c.save();
	c.translate(x, y);
	c.drawImage(img, 0, 0, w, h);
	c.restore();
}

// var endGameDisplay = function(iDxCharac,iDxText){

	// var textX = 350;
	// var textY = 100;

	// //BG
  // imgDrawing(0,0,bgStartEndImg,bgStartEndImg.width,bgStartEndImg.height);
 // //Text 
	// //imgDrawing(textX,textY,textLabelImg,textLabelImg.width/0.9,textLabelImg.height);
 // //Char
	// imgDrawing(0,0,charactersImg[iDxCharac],charactersImg[iDxCharac].width,charactersImg[iDxCharac].height);

  // //Text 
	// imgDrawing(0,0,textImg[iDxText],textImg[iDxText].width,textImg[iDxText].height);

	// //label
  // //imgDrawing(700,500,endLabelImg,endLabelImg.width/2,endLabelImg.height/2);

	// c.save();
	// c.translate(0, 0);
	// c.font = "20px Verdana";
	// c.textAlign = 'center';
	// c.fillStyle = "#F8F8FF";

  // //c.fillText(characArrayText[iDxCharac][0], textX + textLabelImg.width/(0.9*2), textY + textLabelImg.height/2-20);
  // //c.fillText(characArrayText[iDxCharac][1], textX + textLabelImg.width/(0.9*2), textY + textLabelImg.height/2+10);
  // c.restore();
// }



var effectDisplay = function(iDxeffect){
	
	//BG
	  imgDrawing(0,0,effectImg[iDxeffect],effectImg[iDxeffect].width,effectImg[iDxeffect].height);
}

var cardDisplay = function(iDxCard){

  //Card
  imgDrawing(400-(cardImg[iDxCard].width/2),25,cardImg[iDxCard],cardImg[iDxCard].width,cardImg[iDxCard].height);
}


var draw = function() {
	var w = c.canvas.width,
		h = c.canvas.height,
		i;
		
	c.clearRect(0, 0, w, h);
	
		// scoreDisplay.draw();
		// timeDisplay.draw();

};