﻿var ghost = function(size, life, angle, rotation, x, y, vx, vy,imgIdx) {
	this.size = size;
	this.life = life;
	this.angle = angle;
	this.rotation = rotation;
	this.x = x;
	this.y = y;
	this.vx = vx;
	this.vy = vy;
	this.img = imgIdx;
};

var explosion = function(x, y, size, img) {
	this.x = x;
	this.y = y;
	this.size = size;
	this.frame = 15;
	this.img = img || explosionImg;
};

var infoText = function(x, y, time, text, color) {
	this.x = x;
	this.y = y;
	this.time = time;
	this.total = time;
	this.text = text;
	this.color = color;
}

var scoreDisplay = function(x,y,text,color)
{
	this.x = x;
	this.y = y;
	this.text = text;
	this.color = color;
}

var timeDisplay = function(time,text,color)
{
	this.x = 50;
	this.y = 50;
	this.time = time;
	this.total = time;
	this.text = text;
	this.color = color;
}

	
var particle = function(x, y, vx, vy, owner) {
	this.x = x;
	this.y = y;
	this.vx = vx;
	this.vy = vy;
	this.owner = owner;
	this.lifetime = MAX_PARTICLE_TIME;
};

var character = function(x,y,imgIdx)
{
	this.x = x;
	this.y = y;
	this.img = imgIdx;
}

var keyboard = {
	shoot : false,
	left : false,
	right: false,
	up : false,
	down : false,
	press : function(e) {
		manageKeyboard(e.keyCode, true);
	},
	release : function(e) {
		manageKeyboard(e.keyCode, false);
	}
};

var gun = function(id, control, color) {
	this.id = id || 1;					//Sets the ID of the gun (1 is always the own gun)
	this.control = control || keyboard;	//Sets the control of the gun (keyboard is local control)
	this.x = c.canvas.width / 2;		//Centeres start X
	this.y = c.canvas.height - (c.canvas.height *0.5/100);	//Centeres start Y
	this.angle = 0;						//Sets start angle to 0
	this.ammo = MAX_AMMO;				//Sets start ammo to maximum
	this.cooldown = 0;					//Sets start cooldown to minimum
	this.color = (color || 0) % primaryColors.length;//Sets the gun's color
	this.life = MAX_LIFE;				//Sets start life to maximum
	this.boost = 0;						//Sets start boost to minimum
	this.hitByghost = 0;				//Final statistic - how often hit by ghost?
	this.hitghosts = 0;				//Final statistic - how often did kill an ghost?
	this.shotghosts = 0;				//Final statistic - how often did hit an ghost?
	this.hitguns = 0;					//Final statistic - hofen did hit another gun?
};
